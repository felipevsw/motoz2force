.\fastboot.exe oem fb_mode_set
.\fastboot.exe flash partition gpt.bin
.\fastboot.exe flash bootloader bootloader.img
.\fastboot.exe flash modem NON-HLOS.bin
.\fastboot.exe flash fsg fsg.mbn
.\fastboot.exe erase modemst1
.\fastboot.exe erase modemst2
.\fastboot.exe flash bluetooth BTFM.bin
.\fastboot.exe flash dsp adspso.bin
.\fastboot.exe flash logo logo.bin
.\fastboot.exe flash boot boot.img
.\fastboot.exe flash system system.img_sparsechunk.0
.\fastboot.exe flash system system.img_sparsechunk.1
.\fastboot.exe flash system system.img_sparsechunk.2
.\fastboot.exe flash system system.img_sparsechunk.3
.\fastboot.exe flash system system.img_sparsechunk.4
.\fastboot.exe flash system system.img_sparsechunk.5
.\fastboot.exe flash system_b system_b.img_sparsechunk.0
.\fastboot.exe flash system_b system_b.img_sparsechunk.1
.\fastboot.exe flash oem oem.img
.\fastboot.exe erase carrier
.\fastboot.exe erase cache
.\fastboot.exe erase userdata
.\fastboot.exe erase ddr
.\fastboot.exe oem fb_mode_clear
.\fastboot.exe reboot
pause
